// components/signup.js

import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, Platform } from 'react-native';

import { Actions } from 'react-native-router-flux';
import user from '../back4app/user';
import AsyncStorage from '@react-native-community/async-storage';
import installation from '../back4app/installation'


export default class Signup extends Component {
  
  constructor() {
    super();
    this.state = { 
      displayName: '',
      email: '', 
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = async function () {
    if(this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signup!')
    } else {
          const usernameValue = this.state.displayName;
          const passwordValue = this.state.password;
          // Since the signUp method returns a Promise, we need to call it using await
          return await user.signUp(usernameValue, passwordValue)
            .then((createdUser) => {
              // Parse.User.signUp returns the already created ParseUser object if successful
              Alert.alert(
                "Success!",
                `User ${createdUser.get("username")} was successfully created!`
              );
              createdUser.setEmail(this.state.email)
              createdUser.setUsername(this.state.displayName)
              
              installation.set('deviceType',Platform.OS)

              installation.save().then(
                (obj) => {
                  console.log('installation obj is : ' + obj)
                }
              )
              createdUser.set('user_dev',installation.objectId)
              createdUser.save()
              this.setState({
                isLoading: false,
                displayName: '',
                email: '', 
                password: ''
              })
              Actions.dashboard()
              return true;
            })
            .catch((error) => {
              // signUp can fail if any parameter is blank or failed an uniqueness check on the server
              Alert.alert("Error!", error.message);
              error => this.setState({ errorMessage: error.message })
              return false;
            });
          }
  }

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E"/>
        </View>
      )
    }    
    return (
      <View style={styles.container}>  
        <TextInput
          style={styles.inputStyle}
          placeholder="Name"
          value={this.state.displayName}
          onChangeText={(val) => this.updateInputVal(val, 'displayName')}
        />      
        <TextInput
          style={styles.inputStyle}
          placeholder="Email"
          value={this.state.email}
          onChangeText={(val) => this.updateInputVal(val, 'email')}
        />
        <TextInput
          style={styles.inputStyle}
          placeholder="Password"
          value={this.state.password}
          onChangeText={(val) => this.updateInputVal(val, 'password')}
          maxLength={15}
          secureTextEntry={true}
        />   
        <Button
          color="#3740FE"
          title="Signup"
          onPress={() => this.registerUser()}
        />

        <Text 
          style={styles.loginText}
          onPress={() => Actions.login()}>
          Already Registered? Click here to login
        </Text>                          
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    width: '100%',
    marginBottom: 15,
    paddingBottom: 15,
    alignSelf: "center",
    borderColor: "#ccc",
    borderBottomWidth: 1
  },
  loginText: {
    color: '#3740FE',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  }
});