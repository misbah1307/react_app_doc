import React, { useState } from "react";
import Signup from "./Signup";
import Login from "./Login";
import AddMasjid from "./AddMasjid";
import { BottomNavigation } from "react-native-paper";

const SignIn = () => {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: "feeds", title: "Feeds", icon: "message", color: "#3F51B5" },
    { key: "albums", title: "Albums", icon: "album", color: "#009688" },
    { key: "recents", title: "Recents", icon: "history", color: "#795548" },
  ]);

  const renderScene = BottomNavigation.SceneMap({
    feeds: Signup,
    albums: Login,
    recents: AddMasjid,
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};
export default SignIn;
