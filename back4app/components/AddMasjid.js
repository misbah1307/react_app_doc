import React, { useState } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Alert, TextInput, Picker, Button } from 'react-native';
import TimePicker from "react-native-24h-timepicker";
import { Actions } from 'react-native-router-flux';
import user from '../back4app/user';
import Masjids from '../back4app/config';
import Parse, { User } from "parse/react-native.js";



class AddMasjid extends React.Component {
    
    
    state = {
        my_loc:null,
        masjid_name:null,
        fajr:'00:00',
        zohr:'00:00',
        asar:'00:00',
        maghrib:'00:00',
        isha:'00:00',
        namaz:'fajr',
        time:'',
        region: {
            latitude: 10,
            longitude: 10,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
          },
          loading:true,
          success: false,
          suc_msg: ''
    }

    setRegion = (new_region) => {
        this.setState({region:new_region})
    }
    componentDidMount () {
        this.props.navigation.setParams({headerStyle:{backgroundColor:'#800080'}});
        this.props.navigation.setParams({headerTintColor: '#fff'});
        if (this.props.new_location != null){
            this.temp = JSON.parse(this.props.new_location)
            this.setState({my_loc : this.temp});
        }


    }

    
    logout = () =>{
        user.logOut(user.current) .then(() =>{
            alert('Logged out succesfully.')
            console.log('user logged-out ')
            Actions.login()
          })
    }
    onCancel() {
        this.TimePicker.close();
      }
     
      onConfirm(hour, minute) {
        this.setState({ time: `${hour}:${minute}` });
        this.TimePicker.close();
      }

    onFajr = (hr, min) => {
        this.setState({fajr: `${hr}:${min}`})
        this.TimePicker1.close();
    }

    onZohr = (hr, min) => {
        this.setState({zohr: `${hr}:${min}`})
        this.TimePicker2.close();
    }

    onAsar = (hr, min) => {
        this.setState({asar: `${hr}:${min}`})
        this.TimePicker3.close();
    }

    onMaghrib = (hr, min) => {
        this.setState({maghrib: `${hr}:${min}`})
        this.TimePicker4.close();
    }

    onIsha = (hr, min) => {
        this.setState({isha: `${hr}:${min}`})
        this.TimePicker5.close();
    }

    submit = () => {
        if (this.state.fajr == '00:00' || 
        this.state.zohr == '00:00' ||
        this.state.asar == '00:00' ||
        this.state.maghrib == '00:00' ||
        this.state.isha == '00:00' ||
        this.state.masjid_name == null){
            Alert.alert("One of the field is Empty kindly check and fill all the fields")
        }
        else{
            const entry = {
                name: this.state.masjid_name,
                fajr: this.state.fajr,
                zohr: this.state.zohr,
                asar: this.state.asar,
                maghrib: this.state.maghrib,
                isha: this.state.isha
            }
            Alert.alert("Masjid timings",
            "Be sure before you submit ..?",
            [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: 'Yes',
                    onPress: (() => {
                        const masjid = new Masjids()
                        masjid.set('name',this.state.masjid_name);
                        masjid.set('fajr',this.state.fajr);
                        masjid.set('zohr',this.state.zohr);
                        masjid.set('asar',this.state.asar);
                        masjid.set('maghrib',this.state.maghrib);
                        masjid.set('isha',this.state.isha);
                        masjid.set('user',Parse.User.current());
            
                        masjid.save().then((response) =>{
                            this.setState({suc_msg:'masjid is saved in DB'})
                            console.log('masjid saved...!' + response.id)
 
                        })
                        .catch((error) => {
                            console.log(error);
                            Alert(' An error occured ' + error)
                        })
                        
                    })
                }

            ]
            )
           
        }
    }

    masjidName = (text) => {
        this.setState({masjid_name:text});
    }

    render() {
        return (
            <ScrollView>
                
            <View style = {styles.container}>


                
                <TextInput style = {styles.input}
                underlineColorAndroid = "transparent"
                placeholder = "Masjid Name"
                placeholderTextColor = "#9a73ef"
                autoCapitalize = "none"
                onChangeText = {this.masjidName}/>


                <View style={styles.item}>
                    <Text style={styles.text}>FAJR :</Text>
                    <Text style={styles.text}>{this.state.fajr}</Text>
                    <TouchableOpacity
                    onPress={() => this.TimePicker1.open()}
                    style={styles.clock_button}
                    >
                    <Image source = {{uri:'https://t3.ftcdn.net/jpg/02/74/67/60/240_F_274676061_nNeOf3EANU35A9pJcZ1PMT2d4FF6bitE.jpg'}}
                    style = {{ width: 60, height: 40 }}
                    /> 
                    </TouchableOpacity>
                    <TimePicker
                    ref={ref => {
                        this.TimePicker1 = ref;
                    }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => this.onFajr(hour, minute)}
                    />
                </View>

                <View style={styles.item}>
                    <Text style={styles.text}>ZOHR :</Text>
                    <Text style={styles.text}>{this.state.zohr}</Text>
                    <TouchableOpacity
                    onPress={() => this.TimePicker2.open()}
                    style={styles.clock_button}
                    >
                    <Image source = {{uri:'https://t3.ftcdn.net/jpg/02/74/67/60/240_F_274676061_nNeOf3EANU35A9pJcZ1PMT2d4FF6bitE.jpg'}}
                    style = {{ width: 60, height: 40 }}
                    /> 
                    </TouchableOpacity>
                    <TimePicker
                    ref={ref => {
                        this.TimePicker2 = ref;
                    }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => this.onZohr(hour, minute)}
                    />
                </View>

                <View style={styles.item}>
                    <Text style={styles.text}>ASAR :</Text>
                    <Text style={styles.text}>{this.state.asar}</Text>
                    <TouchableOpacity
                    onPress={() => this.TimePicker3.open()}
                    style={styles.clock_button}
                    >
                    <Image source = {{uri:'https://t3.ftcdn.net/jpg/02/74/67/60/240_F_274676061_nNeOf3EANU35A9pJcZ1PMT2d4FF6bitE.jpg'}}
                    style = {{ width: 60, height: 40 }}
                    /> 
                    </TouchableOpacity>
                    <TimePicker
                    ref={ref => {
                        this.TimePicker3 = ref;
                    }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => this.onAsar(hour, minute)}
                    />
                </View>

                <View style={styles.item}>
                    <Text style={styles.text}>MAGHRIB :</Text>
                    <Text style={styles.text}>{this.state.maghrib}</Text>
                    <TouchableOpacity
                    onPress={() => this.TimePicker4.open()}
                    style={styles.clock_button}
                    >
                    <Image source = {{uri:'https://t3.ftcdn.net/jpg/02/74/67/60/240_F_274676061_nNeOf3EANU35A9pJcZ1PMT2d4FF6bitE.jpg'}}
                    style = {{ width: 60, height: 40 }}
                    /> 
                    </TouchableOpacity>
                    <TimePicker
                    ref={ref => {
                        this.TimePicker4 = ref;
                    }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => this.onMaghrib(hour, minute)}
                    />
                </View>

                <View style={styles.item}>
                    <Text style={styles.text}>ISHA :</Text>
                    <Text style={styles.text}>{this.state.isha}</Text>
                    <TouchableOpacity
                    onPress={() => this.TimePicker5.open()}
                    style={styles.clock_button}
                    >
                    <Image source = {{uri:'https://t3.ftcdn.net/jpg/02/74/67/60/240_F_274676061_nNeOf3EANU35A9pJcZ1PMT2d4FF6bitE.jpg'}}
                    style = {{ width: 60, height: 40 }}
                    /> 
                    </TouchableOpacity>
                    <TimePicker
                    ref={ref => {
                        this.TimePicker5 = ref;
                    }}
                    onCancel={() => this.onCancel()}
                    onConfirm={(hour, minute) => this.onIsha(hour, minute)}
                    />
                </View>

                <View style={styles.gps}>
                    <Text>masg:  {this.state.suc_msg} {Parse.User.current().get('username')}</Text>
                </View>

                <TouchableOpacity
                style = {styles.submitButton}
                onPress = {
                    () => this.submit()
                }>
                <Text style = {styles.submitButtonText}> Submit </Text>
                </TouchableOpacity>
                <TouchableOpacity
                style = {styles.submitButton}
                onPress = {
                    () => this.logout()
                }>
                <Text style = {styles.submitButtonText}> Logout </Text>
                </TouchableOpacity>
            </View>
         </ScrollView> 
            
        )
    }
        
    
}

export default AddMasjid;

const styles = StyleSheet.create({
    container: {
        flexDirection:"column",
        justifyContent:"flex-end",
        paddingTop: 23,
        backgroundColor: '#ffccff',
    },
    input: {
       margin: 10,
       height: 60,
       borderColor: '#7a42f4',
       borderWidth: 1,
       backgroundColor: '#fff',
       padding:5,
       borderBottomRightRadius:35,
       borderBottomLeftRadius:35,
       borderTopLeftRadius:35,
       borderTopRightRadius:35,
       fontSize:20,
    },
    item: {
        flexDirection:"row",
        justifyContent:"flex-end",
        alignItems:"flex-end",
        backgroundColor: '#fff',
        borderColor: '#7a42f4',
        borderWidth: 1,
        borderBottomEndRadius:35,
        borderBottomLeftRadius:35,
        borderTopStartRadius:35,
        borderTopEndRadius:35,
        margin: 5,
        padding: 5,
    },
    submitButton: {
       backgroundColor: '#7a42f4',
       padding: 10,
       margin: 15,
       height: 40,
    },
    submitButtonText:{
       color: 'white',
       alignSelf:"center"
    },
    clock_button: {
        paddingRight:20,
        padding: 0,
        borderRadius:30,
    },
    text: {
        paddingRight:50,
        fontSize: 30,
        color:'#9a73ef'
      },
    gps: {
        alignSelf: "center",
        marginTop:20,
        width:200,
        height:200
    },
    

 })