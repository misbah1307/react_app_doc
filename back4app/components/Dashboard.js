// components/dashboard.js

import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, ActionSheetIOS, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import user from '../back4app/user';


export default class Dashboard extends Component {
  constructor() {
    super();
    this.state = { 
      uid: '',
      amLoading: true
    }
  }

  componentDidMount = () =>{
    const User = user.current()
    if (!User) {
        Actions.login()
    }
    else {
        this.setState({amLoading: false})
    }
  }
  signOut = () => {
    user.logOut(user.current) .then(() =>{
      alert('Logged out succesfully.')
      console.log('use logged-out ')
      Actions.login()
    })
    
  }  

  render() {
    if (this.state.amLoading){
        return(
            <View style={styles.preloader}>
              <ActivityIndicator size="large" color="#9E9E9E"/>
            </View>
          )
    }
    else {
          
          return (
            <View style={styles.container}>
              <Text style = {styles.textStyle}>
                Hello, {user.currentAsync().username}
              </Text>
      
              <Button
                color="#3740FE"
                title="Logout"
                onPress={() => this.signOut()}
              />
            </View>
          );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    padding: 35,
    backgroundColor: '#fff'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  }
});