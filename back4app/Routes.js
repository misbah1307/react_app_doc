import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Login from './components/Login'
import Signup from './components/Signup'
import Dashboard from './components/Dashboard'
import AddMasjid from './components/AddMasjid'
import SignIn from './components/mui_signin'



const Routes = () => (
   <Router>
      <Scene>
         <Scene key="signup" component = {Signup} title = "Signup"  hideTabBar/>  
         <Scene key="login" component = {Login} title = "Login" hideTabBar/> 
         <Scene key="dashboard" component = {Dashboard} title = "Dashboard" hideTabBar/>
         <Scene key= "addMasjid" component = {AddMasjid} title = "New  Masjid"></Scene>
         <Scene key= "signin" component = {SignIn} title="Sign In" initial = {true}></Scene>
      </Scene>
   </Router>
)
export default Routes