
import Parse, { User } from "parse/react-native.js";
import AsyncStorage from '@react-native-community/async-storage';
import keys from '../constants/keys';


Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(keys.applicationId, keys.javascriptKey);
Parse.serverURL = keys.serverURL;
user = Parse.User


export default user ; 