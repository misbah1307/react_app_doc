import Parse from "parse/react-native.js";
import AsyncStorage from '@react-native-community/async-storage';
import keys from '../constants/keys';


Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(keys.applicationId, keys.javascriptKey);
Parse.serverURL = keys.serverURL;


const Installation = Parse.Object.extend(Parse.Installation);
const installation = new Installation();

export default installation;