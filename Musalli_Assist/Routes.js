import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Home from './components/Home'
import Login from './components/Login'
import Signup from './components/Signup'
import Dashboard from './components/Dashboard'
import ListView from './components/ListView'
import AddMasjid from './components/AddMasjid'
import Places from './components/Places'


const Routes = () => (
   <Router>
      <Scene>
         <Scene key="signup" component = {Signup} title = "Signup" />  
         <Scene key="login" component = {Login} title = "Login" initial = {true}/> 
         <Scene key="dashboard" component = {Dashboard} title = "Dashboard" />
         
         <Scene key = "home" component = {Home} title = "Home" />
         <Scene key= "listView" component = {ListView} title = "Masajids" />
         <Scene key= "addMasjid" component = {AddMasjid} title = "New  Masjid"></Scene>
         <Scene key= "places" component = {Places} title = "Place" />
      </Scene>
   </Router>
)
export default Routes