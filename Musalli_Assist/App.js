import React from 'react';
import Routes from './Routes.js';
import Parse from "parse/react-native.js";
import AsyncStorage from '@react-native-community/async-storage';
import keys from './constants/keys';

Parse.setAsyncStorage(AsyncStorage);
Parse.initialize(keys.applicationId, keys.javascriptKey);
Parse.serverURL = keys.serverURL;

export default class App extends React.Component  {
  

  componentDidMount = () => {
    createInstallation = async () => {
      const Installation = Parse.Object.extend(Parse.Installation);
      const installation = new Installation();
         
      installation.set('deviceType', Platform.OS);
      await installation.save();
     };
     
     createInstallation();
  }


  render() {
    return (
     <Routes />
    )
  }

}



