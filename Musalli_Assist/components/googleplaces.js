import React, {useRef, useEffect} from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Marker } from 'react-native-maps';
import MapView, { PROVIDER_GOOGLE }  from 'react-native-maps';


class GooglePlacesInput extends React.Component  {

    state= {
        region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
    }
      onRegionChange(region) {
        this.setState({ region });
      }


      render() {
        return (
            <MapView
            provider={PROVIDER_GOOGLE}
            customMapStyle={MapStyle}
            />  

        );
      }



};

export default GooglePlacesInput;