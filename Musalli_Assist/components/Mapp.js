import MapView, { Marker, PROVIDER_GOOGLE} from "react-native-maps";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Alert, TextInput, Picker, Button } from 'react-native';
import React, { useState } from 'react';
export default class Mapp extends React.Component{


    render = () => {
      return (
        <MapView style={{flex:1}} 
        provider={PROVIDER_GOOGLE}
        region={{
          latitude:42.88202,
          longitude:74.582748,
          latitudeDelta:0.0922,
          longitudeDelta:0.0421
        }}
        showsUserLocation  />
      )
    }

}

