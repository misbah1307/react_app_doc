// components/dashboard.js

import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, ActionSheetIOS, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Firebase from '../Firebase/firebase';

export default class Dashboard extends Component {
  constructor() {
    super();
    this.state = { 
      uid: '',
      amLoading: true
    }
  }

  componentDidMount = () =>{
    const user = Firebase.auth().currentUser
    if (!user) {
        Actions.login()
    }
    else {
        this.setState({amLoading: false})
    }
  }
  signOut = () => {
    Firebase.auth().signOut().then(() => {
        console.log('User logged out succesfully')
      Actions.login()
    })
    .catch(error => this.setState({ errorMessage: error.message }))
  }  

  render() {
    if (this.state.amLoading){
        return(
            <View style={styles.preloader}>
              <ActivityIndicator size="large" color="#9E9E9E"/>
            </View>
          )
    }
    else {
        this.state = { 
            displayName: Firebase.auth().currentUser.displayName,
            uid: Firebase.auth().currentUser.uid
          }    
          return (
            <View style={styles.container}>
              <Text style = {styles.textStyle}>
                Hello, {this.state.displayName}
              </Text>
      
              <Button
                color="#3740FE"
                title="Logout"
                onPress={() => this.signOut()}
              />
            </View>
          );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    padding: 35,
    backgroundColor: '#fff'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  }
});