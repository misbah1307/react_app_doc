import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Image } from 'react-native';

class ListView extends React.Component {
    state = {
        masjidList : [
          {'name': 'Al-Quba', 'id': 1, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205 },
          {'name': 'Nimra', 'id': 2, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Abu-Bakr', 'id': 3, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Ameen', 'id': 4, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Hazrat-e-Bilal', 'id': 5, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Eidgah Jadeed', 'id': 6, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Al-Basit', 'id': 7, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Al-Ansar', 'id': 8, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Islahul-Banaat', 'id': 9, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Al-Khair', 'id': 10, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Qamr', 'id': 11, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205},
          {'name': 'Shams', 'id': 12, 'fajr':'5.45', 'zohr':'1.30', 'asar':'5.00', 'maghrib':'6.10', 'isha':'8.15', 'lat':1.203, 'long':1.205}
       ],
       isLogedin : true,
       location: null,
      }

    componentDidMount () {
        this.props.navigation.setParams({headerStyle:{backgroundColor:'#800080'}});
        this.props.navigation.setParams({headerTintColor: '#fff'});
        if (this.props.new_location != null){
            this.temp = JSON.parse(this.props.new_location)
            this.setState({location : this.temp});
        }
        
    }

    render() {
        return(
            <View style={styles.container}>

                <ScrollView >
                {
                    this.state.masjidList.map((item, index) => (
                            
                            <View key = {item.id} style = {styles.masjid}>
                                
                                <View><Text style={styles.name}>{item.name}</Text></View>
                                <View style={styles.second}>
                                    <View style={styles.fajr}><Text>{item.fajr}</Text></View>
                                    <View style={styles.dist}><Text>Distance</Text></View>
                                    <TouchableOpacity style={styles.map}><Image source = {{uri:'https://img.icons8.com/nolan/2x/map-marker.png'}}
                                    style = {{ width: 40, height: 40 }}
                                    /></TouchableOpacity>
                                    <TouchableOpacity><Image source = {{uri:'https://img.icons8.com/windows/2x/Detail-view-of-part.png'}}
                                    style = {{ width: 40, height: 40 }}
                                    /></TouchableOpacity>
                                </View>
                            </View>

                    ))
                }
                </ScrollView>
            </View>
        )
    }
}

export default ListView;

const styles = StyleSheet.create({
    container: {
      flex:10,
      flexDirection: "row",
      backgroundColor: '#ffccff',
      alignItems: "flex-start",
      justifyContent: "space-around",
    },
    masjid: {
        flexDirection:"column",
        justifyContent:"flex-end",
        alignItems:"center",
        padding:10,
        margin: 2,
        borderColor: '#2a4944',
        borderWidth: 1,
        borderBottomEndRadius:35,
        borderBottomLeftRadius:35,
        borderTopStartRadius:35,
        borderTopEndRadius:35,
        backgroundColor: '#fff',

    },
    name:{
        paddingRight:30,
        paddingBottom:5,
        paddingLeft:5,
        fontSize:25,
        color:'purple',
        fontFamily: 'Cochin',


    },
    second:{
        flexDirection:"row",
        justifyContent:"flex-end",
        alignItems:"flex-start",
        paddingTop:20,
    },
    fajr:{
        paddingRight:50,
        color: "#FFFFFF",
        fontSize: 20
    },
    zohr:{

    },
    asar:{

    },
    maghrib:{

    },
    dist:{
        paddingRight:50
    },
    map:{
        paddingRight:50
    },
    details:{

    }

  });