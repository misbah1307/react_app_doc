import React from 'react'
import { TouchableOpacity, Text, Button, Image, View, StyleSheet, Alert, ActivityIndicator} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Geolocation from '@react-native-community/geolocation';



class Home extends React.Component {
    state = {
        initialPosition: null,
        lastPosition: null,
        isLocReady: true,
     }
     watchID = null;
     
    closeActivityIndicator = () => setTimeout(() => this.setState({
        isLocReady: false }), 600)


    componentDidMount = () => {
        this.props.navigation.setParams({headerStyle:{backgroundColor:'#800080'}});
        this.props.navigation.setParams({headerTintColor: '#fff'});
        Geolocation.getCurrentPosition(
            position => {
              const initialPosition = JSON.stringify(position);
              this.setState({initialPosition});
              this.closeActivityIndicator();


            },
            error => Alert.alert('Error', JSON.stringify(error)),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
          );
          this.watchID = Geolocation.watchPosition(position => {
            const lastPosition = JSON.stringify(position);
            this.setState({lastPosition});
            this.closeActivityIndicator();
            this.setState(this.state);

          });
          
     }

    goToAbout = () => {
        Actions.about();
    }

    goToMasjids = () => {
        Actions.listView({location: this.state.initialPosition,new_location: this.state.lastPosition});
        
    }

    addMasjid = () => {
        Actions.addMasjid({location: this.state.initialPosition,new_location: this.state.lastPosition});
    }

    gotoLogin = () => {
       Actions.Login()
        
    }
    render () {
        return (
            <View style = {styles.container}>
                <Image source = {{uri:'https://media.gettyimages.com/photos/islamic-rugs-muslim-praying-carpet-vector-illustration-picture-id1271722543?k=6&m=1271722543&s=612x612&w=0&h=bhreq_xUV--Wd6ztQ1zC4RKb7FAryb0j9fkL0l_cLmk='}}
                style = {{ width: 260, height: 400 }}
                /> 
            <TouchableOpacity style = {{ margin: 50, width: 250 }}>
                <Button onPress = {this.goToMasjids} color = "purple" title = "Masjids"/>
            </TouchableOpacity>
            <TouchableOpacity style = {{ margin: 50, width: 250 }}>
                <Button onPress = {this.addMasjid} color = "purple" title = "New Masjid"/>
            </TouchableOpacity>
            <TouchableOpacity style = {{ margin: 50, width: 250 }}>
                <Button onPress = {this.gotoLogin} color = "purple" title = "Login"/>
            </TouchableOpacity>
            </View>
        )

    }
}
export default Home;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 80
     }
  });
  