import React from 'react'
import { TouchableOpacity, Text, Button } from 'react-native';
import { Actions } from 'react-native-router-flux'



class About extends React.Component {
    goToHome = () => {
        Actions.home()
     }

    goToMasjids = () => {
        Actions.listView()
    }

    render () {
        return (
        <TouchableOpacity style = {{ margin: 128 }}>
            <Text>This is About!</Text>
            <Button onPress = {this.goToHome} color = "red" title = "About" />
            <Button onPress = {this.goToMasjids} color = "Blue" title = "Masjids"/>
        </TouchableOpacity>
        )
    }
}
export default About;