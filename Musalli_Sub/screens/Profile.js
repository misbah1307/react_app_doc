import React, { useState } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import Firebase from '../config/Firebase'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { logout } from '../actions/index'


const isLogged = false
class Profile extends React.Component {

    componentDidMount = () => {
        var user =  Firebase.auth().currentUser
        if (!user) {
            alert('going back to login page')
            this.props.navigation.navigate('Login')
        }
        else {
            isLogged = true
        }
    }

    handleLogout = () => {
        this.props.logout();
        this.props.navigation.navigate('Login')
    }
    render() {
        if (isLogged){
            return (
                <View style={styles.container}>
                    <Text>Profile Screen</Text>
                    <Text>{Firebase.auth().currentUser.displayName}</Text>
                    <Button title="Logout" onPress={this.handleLogout}/>
                    <Text></Text>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        logout: () => dispatch(logout())
    }
}

const mapStateToProps = state => {
    return {
        email: state.email
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);