import Firebase from '../config/Firebase.js';
import db from '../config/db'
// define types

export const UPDATE_EMAIL = 'UPDATE_EMAIL'
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD'
export const LOGIN = 'LOGIN'
export const SIGNUP = 'SIGNUP'
export const LOGOUT = 'LOGOUT'
export const UPDATE_NAME = 'UPDATE_NAME'

export const updateEmail = email => {
    return {
        type: UPDATE_EMAIL,
        payload: email
    }
}

export const updatePassword = password => {
    return {
        type: UPDATE_PASSWORD,
        payload: password
    }
}

export const updateName = name => {
    return {
        type: UPDATE_NAME,
        payload: name
    }
}


export const login = () => {
    return async (dispatch, getState) => {
        try {      
            const { email, password } = getState().user
            console.log(email + '===> ' + password)
            const response = await Firebase.auth().signInWithEmailAndPassword(email, password)
            .then(console.log('Logged in '+ email))
            .catch(error => console.log(error))
            dispatch({ type: LOGIN, payload: response.user })
        } catch (e) {
            console.log(e)
        }
    }
}

export const signup = () => {
    return async (dispatch, getState) => {
        try {
            const { email, password, name } = getState().user
            const response = await Firebase.auth().createUserWithEmailAndPassword(email, password)
            if (response.user.uid) {
                const user = {
                    uid: response.user.uid,
                    email: email,
                    name: name
                }

                db.collection('users')
                    .doc(response.user.uid)
                    .set(user)

                dispatch({ type: SIGNUP, payload: user })
            }
        } catch (e) {
            alert(e)
        }
    }
}

export const logout = () => {
    return async (dispatch, getState) => {
        try{
            const response = await Firebase.auth().signOut()
            .then(console.log('Logged out'))
            dispatch({type: logout, payload: 'Logged out'})
        } catch (e) {
            console.log(e)
        }
    }
}